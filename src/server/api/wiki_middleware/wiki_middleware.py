import requests
from typing import List

from server.api.wiki_middleware.request_wrappers \
    import LinkedTitlesRequestWrapper


class WikiMediaMiddleware:
    _WIKIMEDIA_URL = 'https://en.wikipedia.org/w/api.php?'

    _RANDOM_PAGE_GETTER_ATTRS = {
        'action': 'query',
        'list': 'random',
        'rnnamespace': '0',
        'format': 'json',
    }

    _ADJ_TITLES_GETTER_ATTRS = {
        'action': 'query',
        'titles': None,
        'format': 'json',
        'prop': 'links',
        'pllimit': 'max',
        'plnamespace': '0',
        'indexpageids': True
    }

    def _request_data(self, parameters: dict) -> dict:
        request = requests.get(url=self._WIKIMEDIA_URL, params=parameters)
        return request.json()

    def get_random_page(self) -> str:
        data = self._request_data(self._RANDOM_PAGE_GETTER_ATTRS)
        return data['query']['random'][0]['title']

    def get_adjacent_titles(self, title: str) -> List[str]:
        parameters = self._ADJ_TITLES_GETTER_ATTRS
        parameters['titles'] = title
        result = []
        pl_continue_value = True

        while pl_continue_value is not None:
            data = self._request_data(parameters)
            wrapper = LinkedTitlesRequestWrapper(data)

            pl_continue_value = wrapper.plcontinue
            result += wrapper.linked_titles

            parameters['plcontinue'] = pl_continue_value

        return result
