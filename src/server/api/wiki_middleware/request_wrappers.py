from typing import Optional, List


class LinkedTitlesRequestWrapper:
    def __init__(self, json_data):
        self.data = json_data

    @property
    def plcontinue(self) -> Optional[str]:
        return (self.data['continue']['plcontinue']
                if self.data.get('continue') is not None
                and self.data.get('continue').get('plcontinue') is not None
                else None)

    def _get_pageids(self) -> List[str]:
        return (self.data['query']['pageids']
                if self.data['query']['pageids'] != ['-1'] else [])

    @property
    def linked_titles(self) -> List[str]:
        return [link['title'] for links in
                [self.data['query']['pages'][page_id]['links']
                 for page_id in self._get_pageids()
                 if 'links' in self.data['query']['pages'][page_id]]
                for link in links]
