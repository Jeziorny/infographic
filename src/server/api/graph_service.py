from flask import session, current_app
from flask_api import status
from server.api.config import config
from server.api.http_utils import Utils
from server.api.graph_builder.graph_builder import GraphBuilder
from server.api.graph_generator.graph_generator import GraphGenerator
from server.api.wiki_middleware.wiki_middleware import WikiMediaMiddleware
import threading


def _building_thread_action(title, depth, channel, app):
    callback = GraphBuilder(channel, title, config['DATA_PATH'], app)
    wiki_media_middleware = WikiMediaMiddleware()
    generator = GraphGenerator(wiki_media_middleware)
    generator.build_graph(title, depth, callback)


def _start_building_thread(channel, depth, title):
    app = current_app._get_current_object()
    threading.Thread(
        target=_building_thread_action,
        args=(title, depth, channel, app),
        daemon=True
    ).start()


def build_graph(depth, title=None):
    if 'channel' not in session:
        msg = 'Channel is not created'
        return Utils.error_response(msg, status.HTTP_403_FORBIDDEN)

    channel = session['channel']
    wiki = WikiMediaMiddleware()
    title = title if title else wiki.get_random_page()
    _start_building_thread(channel, depth, title)
    return Utils.send_graph_build_config(title, status.HTTP_200_OK)
