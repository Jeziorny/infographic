import secrets
from flask import session
from flask_api import status
from server.api.http_utils import Utils


def get_channel_name():
    if 'channel' in session:
        return Utils.send_channel_config(
            session['channel'],
            status.HTTP_200_OK)

    channel = secrets.token_urlsafe(32)
    session['channel'] = channel
    return Utils.send_channel_config(channel, status.HTTP_200_OK)
