from functools import wraps, partial
from graph_tool.all import Graph
import asyncio


def async_wrap(func):
    @wraps(func)
    async def run(*args, loop=None, executor=None, **kwargs):
        if loop is None:
            loop = asyncio.get_event_loop()
        f = partial(func, *args, **kwargs)
        return await loop.run_in_executor(executor, f)

    return run


class GraphGenerator:
    def __init__(self, wiki):
        self.wiki = wiki

    @async_wrap
    def fetch_async(self, item):
        return item[1], self.wiki.get_adjacent_titles(item[0])

    def fetch_collection(self, collection):
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        return loop.run_until_complete(asyncio.gather(
            *[self.fetch_async(item) for item in collection]
        ))

    def _expand_graph(self, g, vertices, results):
        new_vertices = []
        for root_vertex, neighbor_ids in results:
            for neighbor in neighbor_ids:
                n_vertex = vertices.get(neighbor)
                if not n_vertex:
                    n_vertex = self._add_vertex(g, vertices, neighbor)
                    new_vertices.append((neighbor, n_vertex))
                g.add_edge(root_vertex, n_vertex)
        return new_vertices

    @staticmethod
    def _add_vertex(graph, vertices, title):
        v = graph.add_vertex()
        graph.vp.title[v] = title
        vertices[title] = v
        return v

    def build_graph(self, start_id, depth_limit, callback):
        g = Graph()
        g.vp.title = g.new_vertex_property("string")

        vertices = {}
        vertex = self._add_vertex(g, vertices, start_id)

        curr_distance = 0
        pending_vertices = [(start_id, vertex)]
        while pending_vertices and curr_distance != depth_limit:
            curr_distance += 1
            results = self.fetch_collection(pending_vertices)
            pending_vertices = self._expand_graph(g, vertices, results)
            callback.call(Graph(g), curr_distance)
