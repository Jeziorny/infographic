from flask_api import status
from flask import send_file
from os import path
from server.api.config import config


class Utils:
    @staticmethod
    def error_response(msg, code):
        body = {'error': msg}
        return body, code

    @staticmethod
    def message_response(msg, code):
        body = {'message': msg}
        return body, code

    @staticmethod
    def send_img(filename):
        file_dir = f"{config['DATA_PATH']}{filename}"

        if not path.exists(file_dir):
            msg = 'Requested resource not found.'
            return Utils.error_response(msg, status.HTTP_404_NOT_FOUND)
        mimetype = 'image/gif'
        body = send_file(f"{config['DATA_RELATIVE_PATH']}{filename}", mimetype)
        return body, status.HTTP_200_OK

    @staticmethod
    def validate_request_params(params, request):
        return all(param in request.args for param in params)

    @staticmethod
    def send_graph_build_config(title, code):
        body = {'title': title}
        return body, code

    @staticmethod
    def send_channel_config(channel, code):
        body = {'channel': channel}
        return body, code
