from flask import Flask
from flask_sse import sse

from server.api.graph_controller import graph
from server.api.app_service import get_channel_name
from flask_api import status
from server.api.config import config
from server.api.http_utils import Utils

app = Flask(__name__)

app.config["REDIS_URL"] = config['REDIS_URL']

app.register_blueprint(graph, url_prefix='/graph')
app.register_blueprint(sse, url_prefix='/stream')

app.secret_key = config['SECRET_KEY']


@app.route('/', methods=['GET'])
def hello():
    msg = 'Welcome! This is the Infographic API.'
    return Utils.message_response(msg, status.HTTP_200_OK)


@app.route('/connect', methods=['GET'])
def create_channel():
    return get_channel_name()
