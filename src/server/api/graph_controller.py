from flask_api import status
from flask import Blueprint, request
from server.api.graph_service import build_graph
from server.api.http_utils import Utils

graph = Blueprint('graph', __name__)


@graph.route('/create', methods=['GET'])
def create_graph():
    depth_param = 'depth'
    title_param = 'title'
    if not Utils.validate_request_params([depth_param], request):
        msg = f'Missing \'{depth_param}\' param.'
        return Utils.error_response(msg, status.HTTP_400_BAD_REQUEST)

    depth = request.args[depth_param]

    if not Utils.validate_request_params([title_param], request):
        return build_graph(depth)
    else:
        return build_graph(depth, request.args[title_param])


@graph.route('/image', methods=['GET'])
def get_image():
    filename_param = 'filename'
    if not Utils.validate_request_params([filename_param], request):
        msg = f'Missing \'{filename_param}\' param.'
        return Utils.error_response(msg, status.HTTP_400_BAD_REQUEST)
    return Utils.send_img(request.args[filename_param])
