from graph_tool.all import Graph, graph_draw
from flask_sse import sse
import threading
import time
import json


def _thread_task(g: Graph, depth, channel, filename, output_dir, app):
    graph_draw(g, output_size=(1800, 1800), output=output_dir)
    with app.app_context():
        message = {"depth": depth, "type": "image", "message": filename}
        sse.publish(json.dumps(message), channel=channel)


class GraphBuilder:
    def __init__(self, channel_name, start_node, work_dir, app):
        self.app = app
        self.channel = channel_name
        self.start_node = str(start_node).replace(' ', '_')
        self.work_dir = work_dir

    def call(self, g: Graph, depth):
        filename = self.start_node + str(time.time()) + ".png"
        output_dir = self.work_dir + filename

        threading.Thread(
            target=_thread_task,
            args=(g, depth, self.channel, filename, output_dir, self.app),
            daemon=True
        ).start()
