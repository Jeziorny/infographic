from unittest.mock import sentinel
from server.api.wiki_middleware.request_wrappers \
    import LinkedTitlesRequestWrapper


def test_plcontinue_property():
    data = {
        'continue': {
            'plcontinue': sentinel.plcontinue
        }
    }

    wrapper = LinkedTitlesRequestWrapper(data)

    assert wrapper.plcontinue == sentinel.plcontinue


def test_linked_titles_property():
    data = {
        'query': {
            'pageids': [
                '902',
            ],
            'pages': {
                '902': {
                    'pageid': 902,
                    'links': [
                        {
                            'title': sentinel.title_1
                        },
                        {
                            'title': sentinel.title_2
                        },
                        {
                            'title': sentinel.title_3
                        },
                    ]
                }
            }
        }
    }
    expected_titles = [sentinel.title_1, sentinel.title_2, sentinel.title_3]

    wrapper = LinkedTitlesRequestWrapper(data)

    assert wrapper.linked_titles == expected_titles
