from unittest.mock import patch, sentinel
from server.api.wiki_middleware.wiki_middleware import WikiMediaMiddleware


source = \
 'server.api.wiki_middleware.wiki_middleware.WikiMediaMiddleware._request_data'


@patch(source)
def test_get_adjacent_titles(request_data_mock):
    requested_title = 'Atom'
    request_data_mock.return_value = {
        'query': {
            'pageids': [
                '902'
            ],
            'pages': {
                '902': {
                    'pageid': 902,
                    'title': requested_title,
                    'links': [
                        {
                            'title': sentinel.title_1
                        },
                        {
                            'title': sentinel.title_2
                        },
                        {
                            'title': sentinel.title_3
                        },
                    ]
                }
            }
        }
    }
    expected_titles = [sentinel.title_1, sentinel.title_2, sentinel.title_3]

    mid = WikiMediaMiddleware()

    assert mid.get_adjacent_titles(requested_title) == expected_titles


@patch(source)
def test_get_random_page(request_data_mock):
    expected_title = sentinel.title
    request_data_mock.return_value = {
        'query': {
            'random': [
                {
                    'title': expected_title
                }
            ],
        },
    }

    mid = WikiMediaMiddleware()

    assert mid.get_random_page() == expected_title
