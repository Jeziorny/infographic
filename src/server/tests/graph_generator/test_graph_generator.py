from unittest.mock import Mock
from graph_tool.all import Graph

from server.api.graph_generator.graph_generator import GraphGenerator


def test_build_graph_empty():
    list_of_ids = [
        1, 2, 3, 4,
    ]

    wiki_mid_mock = Mock()
    wiki_mid_mock.get_adjacent_titles = Mock(side_effect=[[]])

    class CallbackMock:
        def __init__(self):
            self.invokes = 0

        def call(self, g: Graph, dist: int):
            self.invokes += 1
            if self.invokes == 1:
                v = [vert for vert in g.vertices()]
                assert len(v) == 1

    callback = CallbackMock()
    generator = GraphGenerator(wiki_mid_mock)
    generator.build_graph(list_of_ids[0], -1, callback)
    assert callback.invokes == 1


def test_build_graph_wide():
    list_of_ids = [
        1, 2, 3, 4,
    ]

    wiki_mid_mock = Mock()
    wiki_mid_mock.get_adjacent_titles = Mock(
        side_effect=[[list_of_ids[1], list_of_ids[2]], [], []])

    class CallbackMock:
        def __init__(self):
            self.invokes = 0

        def call(self, g: Graph, dist: int):
            self.invokes += 1
            v = [vert for vert in g.vertices()]
            if self.invokes == 1 or self.invokes == 2:
                assert len(v) == 3
                assert g.vertex(0).out_degree() == 2
                assert g.vertex(1).out_degree() == 0
                assert g.vertex(2).out_degree() == 0

    callback = CallbackMock()
    generator = GraphGenerator(wiki_mid_mock)
    generator.build_graph(list_of_ids[0], -1, callback)

    assert callback.invokes == 2


def test_build_graph_deep():
    list_of_ids = [
        1, 2, 3, 4,
    ]

    wiki_mid_mock = Mock()
    wiki_mid_mock.get_adjacent_titles = Mock(
        side_effect=[[list_of_ids[1]], [list_of_ids[2]], [list_of_ids[3]], []])

    class CallbackMock:
        def __init__(self):
            self.invokes = 0

        def call(self, g: Graph, dist: int):
            self.invokes += 1
            v = [vert for vert in g.vertices()]
            if self.invokes == 1:
                assert len(v) == 2
                assert g.vertex(0).out_degree() == 1
                assert g.vertex(1).out_degree() == 0
            elif self.invokes == 2:
                assert len(v) == 3
                assert g.vertex(0).out_degree() == 1
                assert g.vertex(1).out_degree() == 1
                assert g.vertex(2).out_degree() == 0
            elif self.invokes == 3 or self.invokes == 4:
                assert len(v) == 4
                assert g.vertex(0).out_degree() == 1
                assert g.vertex(1).out_degree() == 1
                assert g.vertex(2).out_degree() == 1
                assert g.vertex(3).out_degree() == 0

    callback = CallbackMock()
    generator = GraphGenerator(wiki_mid_mock)
    generator.build_graph(list_of_ids[0], -1, callback)

    assert callback.invokes == 4


def test_build_graph_deep_limit():
    list_of_ids = [
        1, 2, 3, 4,
    ]

    wiki_mid_mock = Mock()
    wiki_mid_mock.get_adjacent_titles = Mock(
        side_effect=[[list_of_ids[1]], [list_of_ids[2]], [list_of_ids[3]], []])

    callback = Mock()
    generator = GraphGenerator(wiki_mid_mock)
    generator.build_graph(list_of_ids[0], 2, callback)

    assert len(callback.call.call_args_list) == 2
