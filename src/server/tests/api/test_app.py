from server.api.app import hello


def test_hello():
    expected = ({'message': 'Welcome! This is the Infographic API.'}, 200)
    assert hello() == expected
