from re import match
from server.api.app import app
from flask_api import status


def test_graph_create_missing_depth():
    expected_status_code = status.HTTP_400_BAD_REQUEST
    expected_data = '{"error":"Missing \'depth\' param."}\n'

    response = app.test_client().get('/graph/create')

    assert response.status_code == expected_status_code
    assert response.data.decode('utf-8') == expected_data


def test_graph_create_unauthorized():
    expected_status_code = status.HTTP_403_FORBIDDEN
    expected_data = '{"error":"Channel is not created"}\n'

    response = app.test_client().get('/graph/create?depth=1')

    assert response.status_code == expected_status_code
    assert response.data.decode('utf-8') == expected_data


def test_graph_create_random_title():
    expected_status_code = status.HTTP_200_OK
    expected_data = '{"title":".+"}\n'

    with app.test_client() as client:
        with client.session_transaction() as session:
            session['channel'] = 'test'
        response = client.get('/graph/create?depth=1')

    assert response.status_code == expected_status_code
    assert match(expected_data, response.data.decode('utf-8'))


def test_graph_create_given_title():
    expected_status_code = status.HTTP_200_OK
    expected_data = '{"title":"cat"}\n'

    with app.test_client() as client:
        with client.session_transaction() as session:
            session['channel'] = 'test'
        response = client.get('/graph/create?depth=1&title=cat')

    assert response.status_code == expected_status_code
    assert response.data.decode('utf-8') == expected_data


def test_get_image_missing_filename():
    expected_status_code = status.HTTP_400_BAD_REQUEST
    expected_data = '{"error":"Missing \'filename\' param."}\n'

    response = app.test_client().get('/graph/image')

    assert response.status_code == expected_status_code
    assert response.data.decode('utf-8') == expected_data
