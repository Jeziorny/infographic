from re import match
from server.api.app import app
from flask_api import status


def test_root():
    expected_status_code = status.HTTP_200_OK
    expected_data = '{"message":"Welcome! This is the Infographic API."}\n'
    response = app.test_client().get('/')
    assert response.status_code == expected_status_code
    assert response.data.decode('utf-8') == expected_data


def test_connect():
    expected_status_code = status.HTTP_200_OK
    expected_data = '{"channel":".+"}\n'
    response = app.test_client().get('/connect')

    assert response.status_code == expected_status_code
    assert match(expected_data, response.data.decode('utf-8'))
