from typing import Callable

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QMessageBox, QDesktopWidget, QPushButton


class AbstractBaseWindow(QWidget):
    def __init__(self, parent=None):
        super(AbstractBaseWindow, self).__init__(parent)

    def closeEvent(self, event):
        response = QMessageBox.question(self, 'Are you sure?',
                                        'Do you want to exit?',
                                        QMessageBox.Yes | QMessageBox.No,
                                        QMessageBox.No)
        if response == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()

    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Escape:
            self.close()

    @staticmethod
    def _resize(widget, width=0, height=0):
        if width != 0:
            widget.resize(width, height)
        qr = widget.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        widget.move(qr.topLeft())

    @staticmethod
    def _set_title(widget):
        widget.setWindowTitle('Infographic')

    @staticmethod
    def _set_button_trigger(button: QPushButton, trigger: Callable):
        button.clicked.connect(trigger)
