from typing import Callable

from PyQt5.QtWidgets import (
    QPushButton, QLabel, QInputDialog, QMessageBox, QLineEdit, QGridLayout
)

from PyQt5.QtCore import Qt

from client.main.gui.abstract_base_window import AbstractBaseWindow


class MenuWindowUI(AbstractBaseWindow):
    def __init__(self, parent=None):
        super(MenuWindowUI, self).__init__(parent)
        self._layout = QGridLayout()
        self._message_label = QLabel('Hello! Choose what you want to do.')
        self._radius_label = QLabel('Graph radius:')
        self._radius_edit = QLineEdit()
        self._radius_edit.setFixedWidth(35)
        self._radius_edit.setAlignment(Qt.AlignCenter)
        self._generate_button = QPushButton('Graph from selected node')
        self._random_button = QPushButton('Graph from random node')
        self._exit_button = QPushButton('Exit')
        self.setLayout(self._layout)
        self._layout.addWidget(self._message_label, 0, 0, Qt.AlignCenter)
        self._layout.addWidget(self._radius_label, 1, 0, Qt.AlignCenter)
        self._layout.addWidget(self._radius_edit, 2, 0, Qt.AlignCenter)
        self._layout.addWidget(self._generate_button, 3, 0)
        self._layout.addWidget(self._random_button, 4, 0)
        self._layout.addWidget(self._exit_button, 5, 0)

        self._set_button_trigger(self._exit_button, self.close)
        self._resize(self, 300, 250)
        self._set_title(self)

    def set_generate_graph_button_trigger(self, trigger):
        self._set_button_trigger(self._generate_button,
                                 self._generate_graph_from_title_runner(
                                     trigger))

    def _generate_graph_from_title_runner(self, trigger: Callable) -> Callable:
        def func():
            title, ok = QInputDialog.getText(self,
                                             'Graph from selected node',
                                             'Enter the title:')
            if ok:
                if title != '':
                    self._title = title
                    self._message_label.setText(
                        f'Generating graph from "{title}"...')
                    trigger()
                else:
                    QMessageBox.warning(self,
                                        'Error',
                                        'You must provide title.')

        return func

    def set_random_graph_button_trigger(self, trigger: Callable):
        self._set_button_trigger(self._random_button,
                                 self._generate_random_graph_runner(trigger))

    def _generate_random_graph_runner(self, trigger: Callable) -> Callable:
        def func():
            self._message_label.setText(
                'Generating graph from random title...')
            trigger()

        return func

    def set_exit_button_trigger(self, trigger: Callable):
        self._set_button_trigger(self._exit_button,
                                 self._generate_exit_button_trigger(trigger))

    def _generate_exit_button_trigger(self, trigger: Callable) -> Callable:
        def func():
            trigger()
            self.close()

        return func

    @property
    def radius(self):
        try:
            r = int(self._radius_edit.text())
            return r
        except ValueError:
            QMessageBox.warning(self,
                                'Error',
                                'Radius should be a unsigned int')

    @property
    def title(self):
        return self._title
