import urllib.request

from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QGridLayout, QLabel, QPushButton

from client.main.gui.abstract_base_window import AbstractBaseWindow


class GraphWindowUI(AbstractBaseWindow):
    def __init__(self, parent=None):
        super(GraphWindowUI, self).__init__(parent)
        self.layout = QGridLayout(self)
        self.label = QLabel(self)
        self.exit_button = QPushButton('Exit')

        self.setLayout(self.layout)
        self.layout.addWidget(self.label, 0, 0, 5, 5)
        self.layout.addWidget(self.exit_button, 5, 3, 1, 1)
        self._set_title(self)
        self._set_button_trigger(self.exit_button, self.close)

    def set_image(self, image_url: str):
        with urllib.request.urlopen(image_url) as url:
            data = url.read()
            pixmap = QPixmap()
            pixmap.loadFromData(data)
            self.label.setPixmap(pixmap)
            self._resize(self)
