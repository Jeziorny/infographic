from client.main.gui.graph_window_ui import GraphWindowUI


class GraphWindow:
    def __init__(self):
        self._graph_windows = {}

    def open_new_graph_window(self, index):
        self._graph_windows[index] = GraphWindowUI()
        self._graph_windows[index].show()

    def load_new_image(self, index, image_path: str):
        self._graph_windows[index].set_image(image_path)
