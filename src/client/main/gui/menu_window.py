from typing import Callable

from client.main.gui.graph_window import GraphWindow
from client.main.gui.menu_window_ui import MenuWindowUI
from client.main.sse_client.client import Client


class MenuWindow:
    def __init__(self):
        self._menu_window_ui = MenuWindowUI()
        self._graph_window = GraphWindow()
        self._sse_client = Client()

    # TODO add graph radius selection
    def start(self):
        self._menu_window_ui.set_generate_graph_button_trigger(
            self._generate_graph_button_trigger()
        )
        self._menu_window_ui.set_random_graph_button_trigger(
            self._generate_random_button_trigger()
        )
        self._menu_window_ui.set_exit_button_trigger(
            self._sse_client.close_connection
        )
        self._menu_window_ui.show()

    def _generate_random_button_trigger(self) -> Callable:

        def func():
            radius = self._menu_window_ui.radius
            if radius is not None:
                for i in range(radius):
                    self._graph_window.open_new_graph_window(i)
                self._sse_client.load_random_page_graph(
                    self._graph_window.load_new_image,
                    radius
                )
        return func

    def _generate_graph_button_trigger(self) -> Callable:

        def func():
            radius = self._menu_window_ui.radius
            title = self._menu_window_ui.title
            if radius is not None:
                for i in range(radius):
                    self._graph_window.open_new_graph_window(i)
                self._sse_client.load_title_page_graph(
                    self._graph_window.load_new_image,
                    title,
                    radius
                )

        return func
