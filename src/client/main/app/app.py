import sys

from PyQt5.QtWidgets import QApplication

from client.main.gui.menu_window import MenuWindow

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MenuWindow()
    window.start()
    sys.exit(app.exec_())
