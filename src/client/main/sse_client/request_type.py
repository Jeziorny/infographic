from enum import Enum


class _Request(Enum):
    # TODO: modify after deploying server endpoint
    CONNECT_WITH_SERVER = 'connect'
    CONNECT_WITH_STREAM = 'stream?channel='
    START_GRAPH_CREATING = 'graph/create'
    GET_IMAGE = '/graph/image?filename='

    @property
    def get_request_path(self):
        # TODO: modify after deploying server endpoint
        return 'http://localhost:5000/' + self.value
