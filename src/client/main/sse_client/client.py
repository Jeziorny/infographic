from typing import Callable

from client.main.sse_client.listener_manager import _EventListenerManager


class Client:
    def __init__(self):
        self._eventListener = None

    def load_random_page_graph(self, callback: Callable, radius: int):
        self.close_connection()
        self._eventListener = _EventListenerManager()
        self._eventListener.start_streaming_for_random_page(
            radius,
            callback
        )

    def load_title_page_graph(self, callback: Callable,
                              title: str,
                              radius: int):
        self.close_connection()
        self._eventListener = _EventListenerManager()
        self._eventListener.start_streaming_for_title(title, radius, callback)

    def close_connection(self):
        if self._eventListener:
            self._eventListener.stop_streaming()
            self._eventListener = None
