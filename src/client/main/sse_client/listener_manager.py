import _thread
import time
from typing import Callable

import requests

from client.main.sse_client.event_listener import _EventListenerThread
from client.main.sse_client.exceptions import EventListenerException
from client.main.sse_client.request_type import _Request


class _EventListenerManager:
    def __init__(self):
        self._thread = None
        self.session = requests.Session()

    def start_streaming_for_random_page(self,
                                        radius: int,
                                        callback: Callable):
        stream_url = self._get_stream_url()
        print(stream_url)
        if self._thread is not None:
            raise EventListenerException(
                'There already exist a running stream'
            )
        self._thread = _EventListenerThread(stream_url, callback)
        self._thread.start()
        _thread.start_new_thread(self._start_streaming, (None, radius))

    def start_streaming_for_title(self, title: str, radius: int,
                                  callback: Callable):
        stream_url = self._get_stream_url()
        print(stream_url)
        if self._thread is not None:
            raise EventListenerException(
                'There already exist a running stream'
            )
        self._thread = _EventListenerThread(stream_url, callback)
        self._thread.start()
        _thread.start_new_thread(self._start_streaming, (title, radius))

    def _get_stream_url(self) -> str:
        request = self.session.get(
            url=_Request.CONNECT_WITH_SERVER.get_request_path
        )
        return _Request.CONNECT_WITH_STREAM.get_request_path\
            + request.json()['channel']

    def _start_streaming(self, page: {None, str}, radius: int) -> str:
        time.sleep(2)
        request = self.session.get(
            url=_Request.START_GRAPH_CREATING.get_request_path,
            params={
                'title': page,
                'depth': radius
            })
        return request.json()['title']

    def stop_streaming(self):
        if self._thread is None:
            raise EventListenerException('There is no running stream')
        self._thread.stop()
        self._thread.join()
        self._thread = None
