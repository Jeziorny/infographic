import json
import logging
import threading
from typing import Callable

from sseclient import SSEClient

from client.main.sse_client.request_type import _Request


class _EventListenerThread(threading.Thread):
    def __init__(self, stream_url, callback: Callable):
        super().__init__()
        self._stream_url = stream_url
        self._callback = callback
        self._stop_event = threading.Event()
        self.daemon = True

    def stop(self):
        self._stop_event.set()

    def run(self):
        self._do_on_event()

    def _do_on_event(self):
        for event in self._get_event(self._stream_url):
            if self._stop_event.is_set():
                logging.info('Stopped listening external')
                break
            x = event.data
            data = json.loads(x)
            self._callback(
                data['depth'] - 1,
                _Request.GET_IMAGE.get_request_path + data['message']
            )

    @staticmethod
    def _get_event(url):
        event_listener = SSEClient(url)
        try:
            for event in event_listener:
                yield event
        except ConnectionError:
            logging.info('Connection closed')
