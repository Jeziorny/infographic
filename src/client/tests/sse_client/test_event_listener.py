import time
from unittest.mock import patch, sentinel

from client.main.sse_client.event_listener import _EventListenerThread
from client.main.sse_client.request_type import _Request


@patch('client.main.sse_client.event_listener._EventListenerThread._get_event')
def test_event_listener_callback(sse_client_mock):
    expected_result_1 = 'X'
    expected_result_2 = 'XX'
    expected_result_3 = 'XXX'
    expected_result = [
        _Request.GET_IMAGE.get_request_path + expected_result_1,
        _Request.GET_IMAGE.get_request_path + expected_result_2,
        _Request.GET_IMAGE.get_request_path + expected_result_3
    ]
    data_to_send = [
        sentinel.data_1,
        sentinel.data_2,
        sentinel.data_3
    ]
    sentinel.data_1.data = f'{{"depth":1, "message":"{expected_result_1}"}}'
    sentinel.data_2.data = f'{{"depth":1, "message":"{expected_result_2}"}}'
    sentinel.data_3.data = f'{{"depth":1, "message":"{expected_result_3}"}}'
    sse_client_mock.return_value = iter(data_to_send)

    tested_result = []

    def test_callback(graph_range, event_data):
        tested_result.append(event_data)

    event_listener = _EventListenerThread(
        sentinel.url,
        test_callback
    )

    event_listener.start()
    event_listener.join()

    assert tested_result == expected_result


@patch('client.main.sse_client.event_listener._EventListenerThread._get_event')
def test_thread_stop(sse_client_mock):
    expected_result_1 = 'X'
    expected_result_2 = 'XX'
    expected_result_3 = 'XXX'
    expected_result = [
        _Request.GET_IMAGE.get_request_path + expected_result_1
    ]
    data_to_send = [
        sentinel.data_1,
        sentinel.data_2,
        sentinel.data_3
    ]
    sentinel.data_1.data = f'{{"depth":1, "message":"{expected_result_1}"}}'
    sentinel.data_2.data = f'{{"depth":1, "message":"{expected_result_2}"}}'
    sentinel.data_3.data = f'{{"depth":1, "message":"{expected_result_3}"}}'
    sse_client_mock.return_value = iter(data_to_send)
    tested_result = []

    def test_callback(graph_range, event_data):
        tested_result.append(event_data)
        time.sleep(1)

    event_listener = _EventListenerThread(
        sentinel.url,
        test_callback
    )

    event_listener.start()
    event_listener.stop()
    event_listener.join()

    assert tested_result == expected_result
