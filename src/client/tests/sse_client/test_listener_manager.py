from unittest.mock import patch, sentinel

import pytest

from client.main.sse_client.listener_manager import _EventListenerManager
from client.main.sse_client.request_type import _Request


def test_stop_streaming_error():
    event_manager = _EventListenerManager()
    with pytest.raises(Exception):
        assert event_manager.stop_streaming()


@patch('client.main.sse_client.event_listener._EventListenerThread.run')
def test_start_streaming_error(thread_start_mock):
    thread_start_mock.return_value = []
    with pytest.raises(Exception):
        event_manager = _EventListenerManager()
        event_manager.start_streaming(
            _Request.GET_RANDOM_PAGE_GRAPH,
            sentinel.radius,
            sentinel.callback
        )
        assert event_manager.start_streaming(
            _Request.GET_RANDOM_PAGE_GRAPH,
            sentinel.radius,
            sentinel.callback
        )
