# Infographic

## Python project


## How to run the project?
Run commands below in the project root directory.

```
docker-compose build
docker-compose up
```

Server is available at *http://0.0.0.0:5000*.

Run commands below in the src directory.

```
pip3 install -r client/requirements.txt
python -m client.main.app.app
```

## Server endpoints

### GET /connect
Creates unique SSE channel name for client.

Response:
 ```
    {
        "channel": "string"
    }
 ```

### GET /stream 
SSE stream. Event template:
```
{
    "depth": int,
    "type": "string",
    "message": "string"
}
```


### GET /graph/create
Starts building a graph for given title.

Parameters:
 - **title** - Title of examined article. If not provided, graph will be built for random title.
 - **depth** - Depth of created graph.

 Response:
 ```
    {
        "title": "string",
    }
 ```

Errors:
 - 400 - "Missing 'depth' param"
 - 403 - "Channel is not created" (At first you need to get channel name from endpoint '/connect')

 ### GET /graph/image
 Returns .png file with graph.
 Parameters:
 - **filename** - Name of file with image of graph. 

Errors:
 - 400 - "Missing 'filename' param."
 - 404 - "Requested resource not found."